/*Classe para objetos do tipo texto, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.textoControle;
import modelo.textoModelo;

@ManagedBean(name="TextBean")
public class TextBean {
	private ArrayList<textoModelo> lista = new textoControle().consultarTodos();
	private int id;
	private String titulo;
	private String descricao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	/* M�todo para inser��o de textos no banco de dados
	 * @param titulo
	 * @param descricao
	 * */ 
	
	public String adicionarTexto() {
		textoModelo mod = new textoModelo();
		textoControle con = new textoControle();
		try {
			mod.setTitulo(this.titulo);
			mod.setDescricao(this.descricao);
			con.inserir(mod);
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "admin.xhtml";
		
	}
	
	/* M�todo para listar dados da tabela
	 * @param id
	 * @return Dados da tabela
	 * */
	
	public ArrayList<textoModelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList<textoModelo> lista) {
		this.lista = lista;
	}
	public textoModelo retornarItem(int id) {
		return lista.get(id);
	}
	

	/* M�todo para fazer a atualiza��o de dados da tabela
	 * @param titulo
	 * @param descricao
	 * */
	
	public String editar() {
		textoModelo mod = new textoModelo();
		textoControle con = new textoControle();
		mod.setId(id);
		mod.setTitulo(titulo);
		mod.setDescricao(descricao);
		if(con.atualizar(mod)) {
			return "index.xhtml";	
		}else {
			return "textoEditar.xhtml";
		}
	}

	/* M�todo para remo��o de dados da tabela
	 * @param id
	 * */
	
	public String deletar(int id) {
		new textoControle().deletar(id);
		return "index.xhtml";
	}

	
	/* M�todo para consultar dados pelo id
	 * @param id
	 * */
	
	public void carregarId(int id) {
		textoModelo con = new textoControle().consultar(id);
		this.setId(con.getId());
		this.setTitulo(con.getTitulo());
		this.setDescricao(con.getDescricao());
	}


}
