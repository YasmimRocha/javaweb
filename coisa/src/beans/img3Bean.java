/*Classe para objetos do tipo imagem3, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

import controle.img3Controle;
import modelo.img3Modelo;

@ManagedBean(name="img3Bean")
public class img3Bean {
	private ArrayList<img3Modelo> lista = new img3Controle().consultarTodos();
	private int id;
	private Part imagem;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	} 

	/* M�todo para inser��o de imagens no banco de dados
	 * @param imagem
	 * */ 
	
	public String pegarImagem() throws IOException {
		img3Modelo mod = new img3Modelo();
		img3Controle con = new img3Controle();
		String pasta = "C:\\Users\\Yasmim\\eclipse-workspace\\coisa\\WebContent\\imgUser";
		InputStream img = imagem.getInputStream();
		String nome = imagem.getSubmittedFileName();
		Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imgUser/"+""+nome;
		mod.setImagem(diretImg);
		con.inserir(mod);
		return "imagem";
	}
	
	/* M�todo para listar imagens da tabela
	 * @param id
	 * @return Dados da tabela
	 * */
	
	public ArrayList<img3Modelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList<img3Modelo> lista) {
		this.lista = lista;
	}
	public img3Modelo retornarItem(int id) {
		return lista.get(id);
	}
	

	/* M�todo para remo��o de imagens da tabela
	 * @param id
	 * */

	public String deletar(int id) {
		new img3Controle().deletar(id);
		return "index.xhtml";
	}

	


}
