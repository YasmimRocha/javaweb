/*Classe para objetos do tipo video, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

import controle.videoControle;
import modelo.videoModelo;

@ManagedBean(name="videoBean")
public class videoBean {
	private ArrayList<videoModelo> lista = new videoControle().consultarTodos();
	private int id;
	private Part video;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return video;
	}
	public void setImagem(Part imagem) {
		this.video = imagem;
	} 
	
	
	/* M�todo para inser��o de v�deos no banco de dados
	 * @param video
	 * */
	
	public String pegarVideo() throws IOException {
		videoModelo mod = new videoModelo();
		videoControle con = new videoControle();
		String pasta = "C:\\Users\\Yasmim\\eclipse-workspace\\coisa\\WebContent\\imgUser";
		InputStream vid = video.getInputStream();
		String nome = video.getSubmittedFileName();
		Files.copy(vid, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imgUser/"+""+nome;
		mod.setVideo(diretImg);
		con.inserir(mod);
		return "video";
	}
	
	/* M�todo para listar dados da tabela
	 * @param id
	 * @return Dados da tabela
	 * */
	
	public ArrayList <videoModelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList <videoModelo> lista) {
		this.lista = lista;
	}
	public videoModelo retornarItem(int id) {
		return lista.get(id);
	}
	

	/* M�todo para remo��o de v�deos da tabela
	 * @param id
	 * */

	public String deletar(int id) {
		new videoControle().deletar(id);
		return "admin.xhtml";
	}

	


}
