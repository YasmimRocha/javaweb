/*classe conexao
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;

import java.sql.Connection; 
import java.sql.SQLException;
//import com.mysql.jdbc.Driver;
import java.sql.DriverManager;

/* M�todo para abrir a conexao
 * @param imagem
 * */

public final class conexao {
	public Connection abrirConexao() {
		Connection connect = null ;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String banco = "cafe";
			String servidor = "jdbc:mysql://localhost/" + banco;
			String user = "root";
			String pwd ="";
			connect = DriverManager.getConnection(servidor, user, pwd);
		}catch(SQLException e ) {
			e.getMessage();
		}catch(Exception e) {
			e.getMessage();
		}
		return connect;	
	}
	
	/* M�todo para fechar a conexao
	 * */
	
	public void fecharConexao(Connection con){
		try {
			con.close();
		}catch(Exception e) {
			e.getMessage();
		}
	}
}
