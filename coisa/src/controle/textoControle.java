/*classe texto
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.textoModelo;
import java.util.ArrayList;

public class textoControle {
	
	/* M�todo para inser��o de textos no banco de dados
	 * @param titulo
	 * @param descricao
	 * */
	
		public boolean inserir(textoModelo txt){
			boolean resultado = false;
			Connection con = new conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO texto(titulo, descricao) VALUES(?,?);");
				ps.setString(1, txt.getTitulo());
				ps.setString(2, txt.getDescricao());
				if(!ps.execute()) {
					resultado = true;
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para fazer a atualiza��o de dados da tabela
		 * @param titulo
		 * @param descricao
		 * */ 
		
		public boolean atualizar(textoModelo txt){
				boolean resultado = false;
				try {
					Connection con = new conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE texto SET titulo=?, descricao=? WHERE id=?");
					ps.setString(1, txt.getTitulo());
					ps.setString(2, txt.getDescricao());
					ps.setInt(3, txt.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		
		/* M�todo para remo��o de dados da tabela
		 * @param id
		 * */
		
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM texto WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para listar dados da tabela
		 * @param id
		 * @return Dados da tabela
		 * */
		
		public ArrayList<textoModelo> consultarTodos(){
			ArrayList<textoModelo> lista = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM texto ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<textoModelo>();
					while(rs.next()){
						textoModelo txt = new textoModelo();
						txt.setTitulo(rs.getString("titulo"));
						txt.setDescricao(rs.getString("descricao"));
						txt.setId(rs.getInt("id"));
						lista.add(txt);
					}
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}

		/* M�todo para consultar dados pelo id
		 * @param id
		 * */
		
		public textoModelo consultar(int id){
			textoModelo txt = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM texto WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					txt = new textoModelo();
					txt.setId(rs.getInt("id"));
					txt.setTitulo(rs.getString("titulo"));
					txt.setDescricao(rs.getString("descricao"));
					new conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return txt;

		}
		

	}
