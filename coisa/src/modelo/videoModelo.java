/*Classe para objetos do tipo video, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */


package modelo;

public class videoModelo {
	private int id;
	private String video;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVideo() {
		return video;
	}
	public void setVideo(String video) {
		this.video = video;
	}

}
