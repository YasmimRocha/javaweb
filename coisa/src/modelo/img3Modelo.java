/*Classe para objetos do tipo imagem3, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package modelo;

public class img3Modelo {
	private int id;
	private String imagem;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

}
