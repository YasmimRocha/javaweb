package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.userControle;
import modelo.userModelo;

@ManagedBean(name="userBean")
public class userBean {
	private ArrayList<userModelo> lista = new userControle().consultarTodos();
	private int id;
	private String nome;
	private String senha;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	
	public String adicionarTexto() {
		userModelo mod = new userModelo();
		userControle con = new userControle();
		try {
			mod.setNome(this.nome);
			mod.setSenha(this.senha);
			con.inserir(mod);
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "user";
		
	}
	
	public ArrayList<userModelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList<userModelo> lista) {
		this.lista = lista;
	}
	public userModelo retornarItem(int id) {
		return lista.get(id);
	}
	

	public String editar() {
		userModelo mod = new userModelo();
		userControle con = new userControle();
		mod.setId(id);
		mod.setNome(nome);
		mod.setSenha(senha);
		if(con.atualizar(mod)) {
			return "user.xhtml";	
		}else {
			return "userEditar.xhtml";
		}
	}

	public String deletar(int id) {
		new userControle().deletar(id);
		return "user.xhtml";
	}

	public void carregarId(int id) {
		userModelo con = new userControle().consultar(id);
		this.setId(con.getId());
		this.setNome(con.getNome());
		this.setSenha(con.getSenha());
	}


}
