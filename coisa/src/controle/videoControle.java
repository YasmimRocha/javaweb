/*classe video
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.videoModelo;
import java.util.ArrayList;

public class videoControle {
	
/* M�todo para inser��o de v�deos no banco de dados
 * @param video
 * */
	
		public boolean inserir(videoModelo vid){
			boolean resultado = false;
			Connection con = new conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO video(video) VALUES(?);");
				ps.setString(1, vid.getVideo());
				if(!ps.execute()) {
					resultado = true;
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		
		public boolean atualizar(videoModelo vid){
				boolean resultado = false;
				try {
					Connection con = new conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE video SET video=?  WHERE id=?");
					ps.setString(1, vid.getVideo());
					ps.setInt(2, vid.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		
		/* M�todo para remo��o de v�deos da tabela
		 * @param id
		 * */
		
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM video WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para listar dados da tabela
		 * @param id
		 * @return Dados da tabela
		 * */
		public ArrayList<videoModelo> consultarTodos(){
			ArrayList<videoModelo> lista = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM video;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<videoModelo>();
					while(rs.next()){
						videoModelo vid = new videoModelo();
						vid.setVideo(rs.getString("video"));
						vid.setId(rs.getInt("id"));
						lista.add(vid);
					}
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}

		/* M�todo para consultar dados pelo id
		 * @param id
		 * */
		public videoModelo consultar(int id){
			videoModelo vid = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM video WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					vid = new videoModelo();
					vid.setId(rs.getInt("id"));
					vid.setVideo(rs.getString("video"));
					new conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return vid;

		}
		

	}
