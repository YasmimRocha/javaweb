/*classe texto3
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.texto3Modelo;
import java.util.ArrayList;

public class texto3Controle {
	
	/* M�todo para inser��o de textos no banco de dados
	 * @param titulo
	 * @param descricao
	 * */
	
		public boolean inserir(texto3Modelo txt){
			boolean resultado = false;
			Connection con = new conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO texto3(titulo, descricao) VALUES(?,?);");
				ps.setString(1, txt.getTitulo());
				ps.setString(2, txt.getDescricao());
				if(!ps.execute()) {
					resultado = true;
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para fazer a atualiza��o de dados da tabela
		 * @param titulo
		 * @param descricao
		 * */ 
		
		public boolean atualizar(texto3Modelo txt){
				boolean resultado = false;
				try {
					Connection con = new conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE texto3 SET titulo=?, descricao=? WHERE id=?");
					ps.setString(1, txt.getTitulo());
					ps.setString(2, txt.getDescricao());
					ps.setInt(3, txt.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		
		/* M�todo para remo��o de dados da tabela
		 * @param id
		 * */
		
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM texto3 WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para listar dados da tabela
		 * @param id
		 * @return Dados da tabela
		 * */
		
		public ArrayList<texto3Modelo> consultarTodos(){
			ArrayList<texto3Modelo> lista = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM texto3 ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<texto3Modelo>();
					while(rs.next()){
						texto3Modelo txt = new texto3Modelo();
						txt.setTitulo(rs.getString("titulo"));
						txt.setDescricao(rs.getString("descricao"));
						txt.setId(rs.getInt("id"));
						lista.add(txt);
					}
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}

		/* M�todo para consultar dados pelo id
		 * @param id
		 * */
		
		public texto3Modelo consultar(int id){
			texto3Modelo txt = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM texto3 WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					txt = new texto3Modelo();
					txt.setId(rs.getInt("id"));
					txt.setTitulo(rs.getString("titulo"));
					txt.setDescricao(rs.getString("descricao"));
					new conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return txt;

		}
		

	}
