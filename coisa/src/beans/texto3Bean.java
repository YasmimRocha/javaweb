/*Classe para objetos do tipo texto3, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package beans;

import java.util.ArrayList;

import javax.faces.bean.ManagedBean;

import controle.texto3Controle;
import modelo.texto3Modelo;

@ManagedBean(name="texto3Bean")
public class texto3Bean {
	private ArrayList<texto3Modelo> lista = new texto3Controle().consultarTodos();
	private int id;
	private String titulo;
	private String descricao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	/* M�todo para inser��o de textos no banco de dados
	 * @param titulo
	 * @param descricao
	 * */ 
	
	public String adicionarTexto() {
		texto3Modelo mod = new texto3Modelo();
		texto3Controle con = new texto3Controle();
		try {
			mod.setTitulo(this.titulo);
			mod.setDescricao(this.descricao);
			con.inserir(mod);
			
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "admin.xhtml";
		
	}
	
	/* M�todo para listar dados da tabela
	 * @param id
	 * @return Dados da tabela
	 * */
	
	public ArrayList<texto3Modelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList<texto3Modelo> lista) {
		this.lista = lista;
	}
	public texto3Modelo retornarItem(int id) {
		return lista.get(id);
	}
	
	/* M�todo para fazer a atualiza��o de dados da tabela
	 * @param titulo
	 * @param descricao
	 * */
	
	public String editar() {
		texto3Modelo mod = new texto3Modelo();
		texto3Controle con = new texto3Controle();
		mod.setId(id);
		mod.setTitulo(titulo);
		mod.setDescricao(descricao);
		if(con.atualizar(mod)) {
			return "index.xhtml";	
		}else {
			return "texto3Editar.xhtml";
		}
	}
	
	/* M�todo para remo��o de dados da tabela
	 * @param id
	 * */

	public String deletar(int id) {
		new texto3Controle().deletar(id);
		return "index.xhtml";
	}

	/* M�todo para consultar dados pelo id
	 * @param id
	 * */
	
	public void carregarId(int id) {
		texto3Modelo con = new texto3Controle().consultar(id);
		this.setId(con.getId());
		this.setTitulo(con.getTitulo());
		this.setDescricao(con.getDescricao());
	}


}
