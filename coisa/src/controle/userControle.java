/*classe usuario
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.userModelo;
import java.util.ArrayList;

public class userControle {
	
		public boolean inserir(userModelo txt){
			boolean resultado = false;
			try {
				Connection con = new conexao().abrirConexao();
				Crypt c = new Crypt();
				String key = "wabbawaabbalubdubshdjahdkjsahoihshazamkrai" + "vanessadamataesnopdogakaskdjue" +
				"euqnaofumoqueriaumcigarrosahgdhsgd" + "xibatanazariajhsadgahja" + "latreltedeixoudecadeiraderodas" + 
				"euestouolhandopralousanessaexatomomento" + "deboniossaodomalmauauauhasugug" + "alienssaodedeosshadgashgdhs";
				String senha= txt.getSenha();
				byte[] re= c.encrypt(senha, key);
				PreparedStatement ps = con.prepareStatement("INSERT INTO user(nome, senha) VALUES(?,?);");
				ps.setString(1, txt.getNome());
				ps.setBytes(2, re);
				if(!ps.execute()) {
					resultado = true;
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return resultado;
		}
		public boolean atualizar(userModelo txt){
				boolean resultado = false;
				try {
					Connection con = new conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE user SET nome=?, senha=? WHERE id=?");
					ps.setString(1, txt.getNome());
					ps.setString(2, txt.getSenha());
					ps.setInt(3, txt.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM user WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		public ArrayList<userModelo> consultarTodos(){
			ArrayList<userModelo> lista = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM user ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<userModelo>();
					while(rs.next()){
						userModelo txt = new userModelo();
						txt.setNome(rs.getString("nome"));
						txt.setSenha(rs.getString("senha"));
						txt.setId(rs.getInt("id"));
						lista.add(txt);
					}
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}

		public userModelo consultar(int id){
			userModelo txt = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM user WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					txt = new userModelo();
					txt.setId(rs.getInt("id"));
					txt.setNome(rs.getString("nome"));
					txt.setSenha(rs.getString("senha"));
					new conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return txt;

		}
		

	}
