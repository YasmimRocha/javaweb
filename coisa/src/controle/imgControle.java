/*classe imagem
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import modelo.imgModelo;
import java.util.ArrayList;

public class imgControle {
	
	/* M�todo para inser��o de imagens no banco de dados
	 * @param imagem
	 * */
	
		public boolean inserir(imgModelo img){
			boolean resultado = false;
			Connection con = new conexao().abrirConexao();
			try {
				PreparedStatement ps = con.prepareStatement("INSERT INTO imagem(imagem) VALUES(?);");
				ps.setString(1, img.getImagem());
				if(!ps.execute()) {
					resultado = true;
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
			return resultado;
		}
		public boolean atualizar(imgModelo img){
				boolean resultado = false;
				try {
					Connection con = new conexao().abrirConexao();
					PreparedStatement ps = con.prepareStatement("UPDATE imagem SET imagem=?  WHERE id=?");
					ps.setString(1, img.getImagem());
					ps.setInt(2, img.getId());
					if(!ps.execute()){
						resultado = true;
					}
					new conexao().fecharConexao(con);
				}catch(SQLException e) {
					System.out.println("Erro ao editar: " + e.getMessage());
				}
			return resultado;
		}
		
		/* M�todo para remo��o de imagens da tabela
		 * @param id
		 * */
		
		public boolean deletar(int id){
			boolean resultado= false;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("DELETE FROM imagem WHERE id=?;");
				ps.setInt(1, id);
				if(!ps.execute()){
					resultado = true;
				}					
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro ao deletar: " + e.getMessage());
			}
			return resultado;
		}
		
		/* M�todo para listar imagens da tabela
		 * @param id
		 * @return Dados da tabela
		 * */
		public ArrayList<imgModelo> consultarTodos(){
			ArrayList<imgModelo> lista = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem ;");
				ResultSet rs = ps.executeQuery();
				if(rs != null){
					lista = new ArrayList<imgModelo>();
					while(rs.next()){
						imgModelo img = new imgModelo();
						img.setImagem(rs.getString("imagem"));
						img.setId(rs.getInt("id"));
						lista.add(img);
					}
				}
				new conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
			return lista;

		}

		/* M�todo para consultar imagens pelo id
		 * @param id
		 * */
		
		public imgModelo consultar(int id){
			imgModelo img = null;
			try {
				Connection con = new conexao().abrirConexao();
				PreparedStatement ps = con.prepareStatement("SELECT * FROM imagem WHERE id=?;");
				ps.setInt(1,id);
				ResultSet rs = ps.executeQuery();
				if(rs != null && rs.next()){
					img = new imgModelo();
					img.setId(rs.getInt("id"));
					img.setImagem(rs.getString("imagem"));
					new conexao().fecharConexao(con);
				}
			}catch(SQLException e) {
				System.out.println("Erro no servidor: " + e.getMessage());
			}
					
			return img;

		}
		

	}
