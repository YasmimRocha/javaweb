/*Classe para objetos do tipo imagem2, onde ser�o contidos, valores e m�todos para o mesmo.
 * @author Yasmim dos Santos Rocha
 * version 1.6
 * */

package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import javax.faces.bean.ManagedBean;
import javax.servlet.http.Part;

import controle.img2Controle;
import modelo.img2Modelo;

@ManagedBean(name="img2Bean")
public class img2Bean {
	private ArrayList<img2Modelo> lista = new img2Controle().consultarTodos();
	private int id;
	private Part imagem;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Part getImagem() {
		return imagem;
	}
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	} 
	
	/* M�todo para inser��o de imagens no banco de dados
	 * @param imagem
	 * */ 
	
	public String pegarImagem() throws IOException {
		img2Modelo mod = new img2Modelo();
		img2Controle con = new img2Controle();
		String pasta = "C:\\Users\\Yasmim\\eclipse-workspace\\coisa\\WebContent\\imgUser";
		InputStream img = imagem.getInputStream();
		String nome = imagem.getSubmittedFileName();
		Files.copy(img, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
		String diretImg = "imgUser/"+""+nome;
		mod.setImagem(diretImg);
		con.inserir(mod);
		return "imagem";
	}
	
	/* M�todo para listar imagens da tabela
	 * @param id
	 * @return Dados da tabela
	 * */
	
	public ArrayList<img2Modelo> getLista() {
		return lista;
	}
	public void setLista(ArrayList<img2Modelo> lista) {
		this.lista = lista;
	}
	public img2Modelo retornarItem(int id) {
		return lista.get(id);
	}
	
	/* M�todo para remo��o de imagens da tabela
	 * @param id
	 * */
	

	public String deletar(int id) {
		new img2Controle().deletar(id);
		return "index.xhtml";
	}

	


}
